package home

import (
	"fmt"
	"net/http"
)

func HomePage(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "Welcome home, this is the home page of the http server written in go.\n")
}

