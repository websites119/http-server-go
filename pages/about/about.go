package about

import (
	"fmt"
	"net/http"
)

func AboutPage(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w,
		"My name is Sage and I am a software developer.\n" +
		"I am interested in creating web and gui applications.\n" +
		"I would want to get a job as a backend or full stack developer.\n" +
		"I am currently a student at college.\n" +
		"My goal is to get some money to afford a new computer.\n")
}
