package main

import (
	"net/http"
	"http-server-go/pages/home"
	"http-server-go/pages/about"
)

func main() {
	http.HandleFunc("/", home.HomePage)
	http.HandleFunc("/about", about.AboutPage)

	http.ListenAndServe(":8080", nil)
}
